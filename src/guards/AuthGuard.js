import store from '../store'

export default (to, from, next) => {
  if (!store.getters.usuario) {
    next({
      path: '/login'
    })
  } else {
    next()
  }
}
