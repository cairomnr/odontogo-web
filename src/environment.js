let variaveis = {
  URL_PADRAO: process.env.NODE_ENV == 'production' ? 'https://odontogo.herokuapp.com/v1/' : 'https://odontogo.herokuapp.com/v1/',
  REQUEST_HEADER: {
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token')
  }
}

export default variaveis
