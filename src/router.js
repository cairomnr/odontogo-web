import Vue from 'vue'
import Router from 'vue-router'

import AuthGuard from './guards/AuthGuard'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: 'login'
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: 'login' */ './views/acesso/Login.vue')
    },
    {
      path: '/admin',
      beforeEnter: AuthGuard,
      component: () =>
        import(/* webpackChunkName: 'admin' */ './views/admin/Admin.vue'),
      children: [
        {
          path: '',
          name: 'admin',
          component: () =>
            import(/* webpackChunkName: 'dashboard' */ './views/admin/Dashboard.vue')
        },
        {
          path: 'perfil',
          name: 'perfil',
          component: () =>
            import(/* webpackChunkName: 'dashboard' */ './views/admin/Perfil.vue')
        },
        {
          path: '/admin/clinica',
          name: 'clinica',
          component: () =>
            import(/* webpackChunkName: 'clinica' */ './views/admin/Clinica.vue')
        },
        {
          path: '/admin/usuario',
          name: 'usuario',
          component: () =>
            import(/* webpackChunkName: 'usuario' */ './views/admin/Usuario.vue')
        },
        {
          path: '/admin/prontuario',
          name: 'prontuario',
          component: () =>
            import(/* webpackChunkName: 'prontuario' */ './views/admin/Prontuario.vue')
        },
        {
          path: '/admin/prontuario-pergunta',
          name: 'prontuario-pergunta',
          component: () =>
            import(/* webpackChunkName: 'prontuario' */ './views/admin/ProntuarioPergunta.vue')
        }
      ]
    }
  ]
})

export default router
