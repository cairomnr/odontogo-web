import Vue from 'vue'
import VueResorce from 'vue-resource'

Vue.use(VueResorce)

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Accept', 'application/json')

  if (localStorage.getItem('token') && localStorage.getItem('token') !== '') {
    request.headers.set('Authorization', localStorage.getItem('token'))
  }

  next()
})

const http = {}

export default http
