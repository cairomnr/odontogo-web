import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usuario: { email: 'cairomnr@hotmail.com' },
    isLoadingLista: false
  },
  mutations: {
    setUsuario(state, usuario) {
      state.usuario = usuario
    },
    setLoadingLista(state, isLoading) {
      state.isLoadingLista = isLoading
    }
  },
  actions: {
    setLoadingLista({ commit }, status) {
      commit('setLoadingLista', status)
    }
  },
  getters: {
    usuario: state => state.usuario,
    isLoadingLista: state => state.isLoadingLista
  }
})
