import Vue from 'vue'
import http from './http'
import App from './App.vue'
import store from './store'
import router from './router'
import VueSwal from 'vue-swal'
import VueTheMask from 'vue-the-mask'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate, { Validator } from 'vee-validate'
import { SpinnerPlugin } from 'bootstrap-vue/es/components'
import VeeValidateMessagesBR from 'vee-validate/dist/locale/pt_BR'

Vue.config.productionTip = false

Validator.localize('pt_BR', VeeValidateMessagesBR)

/** Carregando dependências */
Vue.use(BootstrapVue)
Vue.use(VueSwal)
Vue.use(VueTheMask)
Vue.use(SpinnerPlugin)
Vue.use(VeeValidate, {
  locale: 'pt_BR',
  fieldsBagName: 'fieldsValidation',
  errorBagName: 'errorsValidation'
})

new Vue({
  router,
  store,
  http,
  render: h => h(App)
}).$mount('#app')
